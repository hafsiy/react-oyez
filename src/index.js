import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter, Router, Route } from "react-router-dom";
import { Switch } from "react-router";
import Home from "./Home.js";
import Albums from "./Albums.js";

ReactDOM.render(
  <BrowserRouter>
    <Switch>
      <Route exact path="/" component={Home} />
      <Route path="/Albums/:id" component={Albums} />
    </Switch>
  </BrowserRouter>,
  document.getElementById("root")
);
