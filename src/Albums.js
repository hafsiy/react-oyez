import React, { Component } from "react";
import { withRouter } from "react-router";

class Albums extends Component {
  states = {
    foo: 0
  };

  constructor(props) {
    super(props);
    this.state = {
      albums: [],
      isLoaded: false
    };
  }

  componentDidMount() {
    fetch(
      "https://jsonplaceholder.typicode.com/albums/" +
      this.props.match.params.id +
        "/photos"
    )
      .then(response => response.json())
      .then(data =>
        this.setState({
          isLoaded: true,
          albums: data
        })
      );
    console.log(this.props.match.params.id);
  }

  render() {
    var { isLoaded, albums } = this.state;
    if (!isLoaded) {
      return <div>Loading...</div>;
    } else {
      return (
        <div className="root">
          <ul>
            {albums.map(album => (
              <li key={album.id}>
                <img src={album.url} alt="" />
              </li>
            ))}
          </ul>
        </div>
      );
    }
  }
}

export default withRouter(Albums);
