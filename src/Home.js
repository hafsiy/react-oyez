import React, { Component } from "react";
import { browserHistory } from "react-router";
import Albums from "./Albums.js";
import { withRouter } from "react-router";

class Home extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      albums: [],
      isLoaded: false,
    };
  }

  componentDidMount() {
      fetch("https://jsonplaceholder.typicode.com/albums")
        .then(response => response.json())
        .then(data =>
          this.setState({
            albums: data,
            isLoaded: true,
          })
        );
  }
  handleClick(id) {
  // alert(id);
  this.props.history.push('/Albums/'+id);
  
  }

  render() {
    var { isLoaded, albums } = this.state;
    if (!isLoaded) {
      return <div>Loading...</div>;
    } else {
      const viewAlbums = albums.map((el, index) => {
        return (
          <ul>
            {albums.map(album => (
              <li key={album.id}>
                {album.title}
                <button onClick={() => this.handleClick(album.id)}>
                  check pictures
                </button>
              </li>
            ))}
          </ul>
        );
      });

      return (
        <div className="root">
          <div>{viewAlbums}</div>
        </div>
      );
    }
  }
}

export default withRouter(Home);
